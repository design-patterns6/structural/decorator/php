<?php

declare(strict_types=1);

namespace Decorator\Tests;

use Decorator\Book;
use Decorator\BookTitleDecorator;
use Decorator\BookTitleExclaimDecorator;
use Decorator\BookTitleStarDecorator;
use PHPUnit\Framework\TestCase;

class DecoratorTest extends TestCase
{
    private const BASE_TITLE = 'Gamma, Helm, Johnson, and Vlissides';
    private BookTitleDecorator $decorator;

    protected function setUp(): void
    {
        parent::setUp();

        $patternBook = new Book(titleIn: self::BASE_TITLE, authorIn: 'Design Patterns');
        $this->decorator = new BookTitleDecorator(bookIn: $patternBook);
    }

    public function testBookTitleExclaimDecoratorShouldAddExclamationMarksAtBeginAndEndOfTheTitle(): void
    {
        // GIVEN
        $exclaimDecorator = new BookTitleExclaimDecorator(btdIn: $this->decorator);
        self::assertEquals(expected: self::BASE_TITLE, actual: $this->decorator->showTitle());
        // WHEN
        $exclaimDecorator->exclaimTitle();
        // THEN
        self::assertEquals(expected: '!Gamma, Helm, Johnson, and Vlissides!', actual: $this->decorator->showTitle());
    }

    public function testBookTitleStarDecoratorShouldAddStarsBetweenWordsInTitle(): void
    {
        // GIVEN
        $starDecorator = new BookTitleStarDecorator(btdIn: $this->decorator);
        self::assertEquals(expected: self::BASE_TITLE, actual: $this->decorator->showTitle());
        // WHEN
        $starDecorator->starTitle();
        // THEN
        self::assertEquals(expected: 'Gamma,*Helm,*Johnson,*and*Vlissides', actual: $this->decorator->showTitle());
    }

    public function testBookTitleDecoratorShouldResetTitle(): void
    {
        // GIVEN
        $exclaimDecorator = new BookTitleExclaimDecorator(btdIn: $this->decorator);
        $starDecorator = new BookTitleStarDecorator(btdIn: $this->decorator);
        $exclaimDecorator->exclaimTitle();
        $starDecorator->starTitle();
        // WHEN
        $this->decorator->resetTitle();
        // THEN
        self::assertEquals(expected: self::BASE_TITLE, actual: $this->decorator->showTitle());
    }
}
