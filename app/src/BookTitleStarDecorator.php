<?php

declare(strict_types=1);

namespace Decorator;

class BookTitleStarDecorator extends BookTitleDecorator
{
    private BookTitleDecorator $btd;

    public function __construct(BookTitleDecorator $btdIn)
    {
        $this->btd = $btdIn;
    }

    public function starTitle(): void
    {
        $this->btd->title = str_replace(' ', '*', $this->btd->title);
    }
}
