<?php

declare(strict_types=1);

namespace Decorator;

class Book
{
    private string $author;
    private string $title;

    public function __construct(string $titleIn, string $authorIn)
    {
        $this->author = $authorIn;
        $this->title = $titleIn;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
