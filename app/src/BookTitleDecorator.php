<?php

declare(strict_types=1);

namespace Decorator;

class BookTitleDecorator
{
    protected Book $book;
    protected string $title;

    public function __construct(Book $bookIn)
    {
        $this->book = $bookIn;
        $this->resetTitle();
    }

    public function resetTitle(): void
    {
        $this->title = $this->book->getTitle();
    }

    public function showTitle(): string
    {
        return $this->title;
    }
}
