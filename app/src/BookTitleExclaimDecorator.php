<?php

declare(strict_types=1);

namespace Decorator;

class BookTitleExclaimDecorator extends BookTitleDecorator
{
    private BookTitleDecorator $btd;

    public function __construct(BookTitleDecorator $btdIn)
    {
        $this->btd = $btdIn;
    }

    public function exclaimTitle(): void
    {
        $this->btd->title = '!'.$this->btd->title.'!';
    }
}
